# Magneto Driveway Alarm

This circuit is designed to work as a vehicle detector using only a magnetometer to watch for nearby metal objects. Experimentation is still ongoing to increase sensitivity and eliminate false positives. Works great in long driveways with the LoRa radios range, and does not false-alert on people, deer, birds, wind, etc.

Early tests used a QMC5883 sensor and showed great promise in the idea, but the sensor was always jittery enough it made it impossible to average out false readings without averaging out the valid ones as well.

## Parts needed
### Hardware
The circuit is simple and not many parts are required. This prototype was made with wire-wrap on perfboard but should work fine on breadboard or with jumper wires, whatever you've got.

* Arduino pro-mini 8Mhz/3.3volt
* GLY3110 magnetometer breakout (available from [sparkfun](https://www.sparkfun.com/products/12670) or [ebay](https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2334524.m570.l1313.TR0.TRC0.A0.H0.XMAG3110.TRS1&_nkw=MAG3110&_sacat=0&LH_TitleDesc=0&_osacat=0&_odkw=sensor+3110&LH_TitleDesc=0))
* LoRa sx1278 radio (available in several frequencies from several suppliers such as adafruit and electrodragon)
  * There are also several boards that behave like arduino Nanos or Pro-minis with LoRa radios built in (i.e. [electrodragon](https://www.electrodragon.com/product/atmega328p-arduino-plus-lora-sx1278-board-loraduino/))
* 3k3Ohm resistors- external pullups are essential here, as the built-in 20k of the ATMEGA328 are not sufficient with cable lengths get long. Without them you sensor can't be hardly more than a foot away from the MCU, with them the signal is pretty clean with a test piece of 12ft telephone wire.


### Software
Dependencies documentation is here:
* [the sparkfun library](https://github.com/sparkfun/SparkFun_MAG3110_Breakout_Board_Arduino_Library)
* [Sandeep's LoRa library](https://github.com/sandeepmistry/arduino-LoRa)
A copy of these libraries is included in the firmware/lib/ folder.

## Wiring
![Wirewrapped on proto board](/hardware/wirewrapped.JPG)
Wiring is straightforward, and usable with just about any arduino version. The pro mini 8mhz works great with the GLY3110 magnetometer and the LoRa radio without any level shifting needed. If using a 5v arduino, a bidirectional level-shifter will be needed for the I2C lines. A 1k-2.2k resistor divider works great to interface the LoRa radios to 5v arduinos via SPI.

The sensor was buried in the road and connected to the arduino with outdoor telephone wire about 12ft long. The I2C seems to be working fine over this un-shielded cable, but if any interference issues come up I will likely try slowing down the I2C speed.

## Sensor testing
Testing of the old QMC sensor could detect a small car moving slowly up to about 5 feet to either side of the sensor. If the sensor is buried in the middle of a driveway it's range would be the width of the vehicle plus about 10 feet. When driving over the sensor it also picked up an ATV and a riding lawnmower with ease.

With a minimum deviation set to trigger the alert, the system never failed to detect a vehicle- but it did frequently false alarm on it's own rather extreme swings in value, even with averaging.  

The current version uses the GLY3110 and is being tested on the bench. Sensor jitter is reduced by reading x, y, and z readings into three buffers every 50ms, averaging the entire buffer, and comparing that to a previous average. This will account for any slight drift in the sensor with time or temperature, as well as a sort of 'auto zero'- if a vehicle parks on the sensor, it would only alarm once until the vehicle moves again, preventing constant triggering on a parked vehicle.  

