EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3900 800  0    50   ~ 0
remember to update footprint for newer pro-minis with the SDA/SCL pins in a different place\n
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F299591
P 2450 2775
F 0 "J1" H 2368 2450 50  0000 C CNN
F 1 "BeeperConnection" H 2368 2541 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2450 2775 50  0001 C CNN
F 3 "~" H 2450 2775 50  0001 C CNN
	1    2450 2775
	-1   0    0    1   
$EndComp
$Comp
L Transistor_BJT:2N3904 Q1
U 1 1 5F299BA6
P 2750 3100
F 0 "Q1" H 2940 3146 50  0000 L CNN
F 1 "2N3904" H 2940 3055 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2950 3025 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 2750 3100 50  0001 L CNN
	1    2750 3100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack J2
U 1 1 5F29A0DC
P 2575 1800
F 0 "J2" H 2632 2125 50  0000 C CNN
F 1 "Barrel_Jack" H 2632 2034 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 2625 1760 50  0001 C CNN
F 3 "~" H 2625 1760 50  0001 C CNN
	1    2575 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0101
U 1 1 5F29B2CC
P 2950 1650
F 0 "#PWR0101" H 2950 1500 50  0001 C CNN
F 1 "+12V" H 2965 1823 50  0000 C CNN
F 2 "" H 2950 1650 50  0001 C CNN
F 3 "" H 2950 1650 50  0001 C CNN
	1    2950 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2875 1700 2950 1700
Wire Wire Line
	2950 1700 2950 1650
$Comp
L power:GND #PWR0102
U 1 1 5F29B742
P 2950 1950
F 0 "#PWR0102" H 2950 1700 50  0001 C CNN
F 1 "GND" H 2955 1777 50  0000 C CNN
F 2 "" H 2950 1950 50  0001 C CNN
F 3 "" H 2950 1950 50  0001 C CNN
	1    2950 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2875 1900 2950 1900
Wire Wire Line
	2950 1900 2950 1950
$Comp
L MainWorking:Sx1278_LORA_EE1 U3
U 1 1 5F29F1FB
P 7800 2100
F 0 "U3" H 8050 1800 50  0000 C CNN
F 1 "Sx1278_LORA_EE1" H 8100 1700 50  0000 C CNN
F 2 "MainWorking:LORA_SX1278_Module" H 7800 2100 50  0001 C CNN
F 3 "" H 7800 2100 50  0001 C CNN
	1    7800 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F29FBFD
P 3350 3725
F 0 "D1" H 3343 3942 50  0000 C CNN
F 1 "LED" H 3343 3851 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 3350 3725 50  0001 C CNN
F 3 "~" H 3350 3725 50  0001 C CNN
	1    3350 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5F29FE4C
P 3350 4075
F 0 "D2" H 3343 4292 50  0000 C CNN
F 1 "LED" H 3343 4201 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 3350 4075 50  0001 C CNN
F 3 "~" H 3350 4075 50  0001 C CNN
	1    3350 4075
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5F2A00CA
P 3350 4450
F 0 "D3" H 3343 4667 50  0000 C CNN
F 1 "LED" H 3343 4576 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 3350 4450 50  0001 C CNN
F 3 "~" H 3350 4450 50  0001 C CNN
	1    3350 4450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5F2A0786
P 4100 3700
F 0 "SW1" H 4100 3985 50  0000 C CNN
F 1 "GateOpen" H 4100 3894 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H8mm" H 4100 3900 50  0001 C CNN
F 3 "~" H 4100 3900 50  0001 C CNN
	1    4100 3700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5F2DACAB
P 4100 4050
F 0 "SW2" H 4100 4335 50  0000 C CNN
F 1 "GateClose" H 4100 4244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H8mm" H 4100 4250 50  0001 C CNN
F 3 "~" H 4100 4250 50  0001 C CNN
	1    4100 4050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5F2DAFF3
P 4100 4425
F 0 "SW3" H 4100 4710 50  0000 C CNN
F 1 "DinnerBell" H 4100 4619 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H8mm" H 4100 4625 50  0001 C CNN
F 3 "~" H 4100 4625 50  0001 C CNN
	1    4100 4425
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0103
U 1 1 5F2E3D58
P 2950 2550
F 0 "#PWR0103" H 2950 2400 50  0001 C CNN
F 1 "+12V" H 2965 2723 50  0000 C CNN
F 2 "" H 2950 2550 50  0001 C CNN
F 3 "" H 2950 2550 50  0001 C CNN
	1    2950 2550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F2E4051
P 2650 3400
F 0 "#PWR0104" H 2650 3150 50  0001 C CNN
F 1 "GND" H 2655 3227 50  0000 C CNN
F 2 "" H 2650 3400 50  0001 C CNN
F 3 "" H 2650 3400 50  0001 C CNN
	1    2650 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3300 2650 3400
Wire Wire Line
	2650 2775 2650 2900
Wire Wire Line
	2950 2550 2950 2675
Wire Wire Line
	2950 2675 2650 2675
Text Label 5950 4450 0    50   ~ 0
beeper
Text Label 5950 4300 0    50   ~ 0
mosi
Text Label 5950 4150 0    50   ~ 0
miso
Text Label 5950 4000 0    50   ~ 0
sck
Text Label 5050 4450 2    50   ~ 0
cs
Text Label 5050 3400 2    50   ~ 0
irq
Text Label 5050 4300 2    50   ~ 0
rst
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5F2E6C4D
P 4700 2800
F 0 "J3" H 4625 2900 50  0000 C CNN
F 1 "Serial" H 4625 2975 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4700 2800 50  0001 C CNN
F 3 "~" H 4700 2800 50  0001 C CNN
	1    4700 2800
	-1   0    0    -1  
$EndComp
Text Label 8400 2800 0    50   ~ 0
irq
Text Label 7200 2750 2    50   ~ 0
rst
Text Label 7200 2900 2    50   ~ 0
cs
Text Label 7200 3050 2    50   ~ 0
sck
Text Label 7200 3200 2    50   ~ 0
mosi
Text Label 7200 3350 2    50   ~ 0
miso
$Comp
L power:+3.3V #PWR0105
U 1 1 5F2EBBD6
P 7650 2350
F 0 "#PWR0105" H 7650 2200 50  0001 C CNN
F 1 "+3.3V" H 7665 2523 50  0000 C CNN
F 2 "" H 7650 2350 50  0001 C CNN
F 3 "" H 7650 2350 50  0001 C CNN
	1    7650 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0106
U 1 1 5F2EBE51
P 6375 3000
F 0 "#PWR0106" H 6375 2850 50  0001 C CNN
F 1 "+3.3V" H 6390 3173 50  0000 C CNN
F 2 "" H 6375 3000 50  0001 C CNN
F 3 "" H 6375 3000 50  0001 C CNN
	1    6375 3000
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AMS1117-3.3 U1
U 1 1 5F2ECEA7
P 3925 1700
F 0 "U1" H 3925 1942 50  0000 C CNN
F 1 "AMS1117-3.3" H 3925 1851 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 3925 1900 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 4025 1450 50  0001 C CNN
	1    3925 1700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F2EE01D
P 3925 2100
F 0 "#PWR0107" H 3925 1850 50  0001 C CNN
F 1 "GND" H 3930 1927 50  0000 C CNN
F 2 "" H 3925 2100 50  0001 C CNN
F 3 "" H 3925 2100 50  0001 C CNN
	1    3925 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5F2EE68C
P 3475 1850
F 0 "C2" H 3590 1896 50  0000 L CNN
F 1 "C" H 3590 1805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D7.5mm_W5.0mm_P5.00mm" H 3513 1700 50  0001 C CNN
F 3 "~" H 3475 1850 50  0001 C CNN
	1    3475 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5F2EE937
P 4375 1850
F 0 "C3" H 4490 1896 50  0000 L CNN
F 1 "C" H 4490 1805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D7.5mm_W5.0mm_P5.00mm" H 4413 1700 50  0001 C CNN
F 3 "~" H 4375 1850 50  0001 C CNN
	1    4375 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 1700 3200 1700
Connection ~ 2950 1700
Connection ~ 3475 1700
Wire Wire Line
	3475 1700 3625 1700
Wire Wire Line
	4225 1700 4375 1700
Wire Wire Line
	3475 2000 3925 2000
Connection ~ 3925 2000
Wire Wire Line
	3925 2000 4375 2000
Wire Wire Line
	3925 2000 3925 2100
$Comp
L power:+3.3V #PWR0108
U 1 1 5F2F0DC8
P 4500 1700
F 0 "#PWR0108" H 4500 1550 50  0001 C CNN
F 1 "+3.3V" H 4515 1873 50  0000 C CNN
F 2 "" H 4500 1700 50  0001 C CNN
F 3 "" H 4500 1700 50  0001 C CNN
	1    4500 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 1700 4375 1700
Connection ~ 4375 1700
$Comp
L power:GND #PWR0109
U 1 1 5F309152
P 3900 4925
F 0 "#PWR0109" H 3900 4675 50  0001 C CNN
F 1 "GND" H 3905 4752 50  0000 C CNN
F 2 "" H 3900 4925 50  0001 C CNN
F 3 "" H 3900 4925 50  0001 C CNN
	1    3900 4925
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 3700 3900 4050
Connection ~ 3900 4050
Wire Wire Line
	3900 4050 3900 4425
Connection ~ 3900 4425
$Comp
L Device:Antenna AE1
U 1 1 5F30C4F4
P 8700 2425
F 0 "AE1" H 8780 2414 50  0000 L CNN
F 1 "Antenna" H 8780 2323 50  0000 L CNN
F 2 "Connector_Coaxial:SMA_Molex_73251-1153_EdgeMount_Horizontal" H 8700 2425 50  0001 C CNN
F 3 "~" H 8700 2425 50  0001 C CNN
	1    8700 2425
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2650 8700 2650
Wire Wire Line
	8700 2650 8700 2625
NoConn ~ 8400 2900
NoConn ~ 8400 3000
NoConn ~ 8400 3100
NoConn ~ 8400 3200
NoConn ~ 8400 3300
$Comp
L power:GND #PWR0110
U 1 1 5F30EB25
P 7900 3850
F 0 "#PWR0110" H 7900 3600 50  0001 C CNN
F 1 "GND" H 7905 3677 50  0000 C CNN
F 2 "" H 7900 3850 50  0001 C CNN
F 3 "" H 7900 3850 50  0001 C CNN
	1    7900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3750 7800 3750
Connection ~ 7800 3750
Wire Wire Line
	7800 3750 7900 3750
Wire Wire Line
	7900 3850 7900 3750
Connection ~ 7900 3750
$Comp
L power:GND #PWR0111
U 1 1 5F30FDAB
P 6250 2950
F 0 "#PWR0111" H 6250 2700 50  0001 C CNN
F 1 "GND" H 6255 2777 50  0000 C CNN
F 2 "" H 6250 2950 50  0001 C CNN
F 3 "" H 6250 2950 50  0001 C CNN
	1    6250 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 2950 6250 2950
$Comp
L power:GND #PWR0112
U 1 1 5F31098E
P 4675 3250
F 0 "#PWR0112" H 4675 3000 50  0001 C CNN
F 1 "GND" H 4680 3077 50  0000 C CNN
F 2 "" H 4675 3250 50  0001 C CNN
F 3 "" H 4675 3250 50  0001 C CNN
	1    4675 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3250 4675 3250
NoConn ~ 5300 4650
NoConn ~ 5400 4650
NoConn ~ 5550 4650
NoConn ~ 5650 4650
$Comp
L Device:R R1
U 1 1 5F31D3BA
P 2975 3725
F 0 "R1" V 2768 3725 50  0000 C CNN
F 1 "R" V 2859 3725 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2905 3725 50  0001 C CNN
F 3 "~" H 2975 3725 50  0001 C CNN
	1    2975 3725
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5F31D7D9
P 2975 4075
F 0 "R2" V 2768 4075 50  0000 C CNN
F 1 "R" V 2859 4075 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2905 4075 50  0001 C CNN
F 3 "~" H 2975 4075 50  0001 C CNN
	1    2975 4075
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5F31DBC9
P 2975 4450
F 0 "R3" V 2768 4450 50  0000 C CNN
F 1 "R" V 2859 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 2905 4450 50  0001 C CNN
F 3 "~" H 2975 4450 50  0001 C CNN
	1    2975 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 3725 3125 3725
Wire Wire Line
	3125 4075 3200 4075
Wire Wire Line
	3125 4450 3200 4450
$Comp
L power:GND #PWR0113
U 1 1 5F320068
P 2825 4625
F 0 "#PWR0113" H 2825 4375 50  0001 C CNN
F 1 "GND" H 2830 4452 50  0000 C CNN
F 2 "" H 2825 4625 50  0001 C CNN
F 3 "" H 2825 4625 50  0001 C CNN
	1    2825 4625
	1    0    0    -1  
$EndComp
Wire Wire Line
	2825 4625 2825 4450
Connection ~ 2825 4075
Wire Wire Line
	2825 4075 2825 3725
Connection ~ 2825 4450
Wire Wire Line
	2825 4450 2825 4075
Text Label 3500 3725 0    50   ~ 0
drvLED
Text Label 3500 4075 0    50   ~ 0
TXLED
Text Label 3500 4450 0    50   ~ 0
RXLED
Text Label 5050 3850 2    50   ~ 0
drvLED
Text Label 5050 4000 2    50   ~ 0
TXLED
Text Label 5050 4150 2    50   ~ 0
RXLED
Text Label 4300 4050 0    50   ~ 0
gateclose
Text Label 4300 4425 0    50   ~ 0
dinnerbell
Text Label 4300 3700 0    50   ~ 0
gateopen
Text Label 5950 3400 0    50   ~ 0
gateopen
Text Label 5950 3550 0    50   ~ 0
gateclose
Text Label 5950 3700 0    50   ~ 0
dinnerbell
Wire Wire Line
	3900 4425 3900 4770
Wire Wire Line
	6375 3000 6375 3250
Wire Wire Line
	6375 3250 5950 3250
Wire Wire Line
	4900 2800 5050 2800
Wire Wire Line
	4900 2900 5000 2900
Wire Wire Line
	5000 2900 5000 2950
Wire Wire Line
	5000 2950 5050 2950
Text Label 2950 3100 0    50   ~ 0
beeper
$Comp
L Device:C C1
U 1 1 5F34262C
P 3200 1850
F 0 "C1" H 3315 1896 50  0000 L CNN
F 1 "C" H 3315 1805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D7.5mm_W5.0mm_P5.00mm" H 3238 1700 50  0001 C CNN
F 3 "~" H 3200 1850 50  0001 C CNN
	1    3200 1850
	1    0    0    -1  
$EndComp
Connection ~ 3200 1700
Wire Wire Line
	3200 1700 3475 1700
Wire Wire Line
	3200 2000 3475 2000
Connection ~ 3475 2000
$Comp
L Mechanical:MountingHole H1
U 1 1 5F2FA4C7
P 825 650
F 0 "H1" H 925 696 50  0000 L CNN
F 1 "MountingHole" H 925 605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 825 650 50  0001 C CNN
F 3 "~" H 825 650 50  0001 C CNN
	1    825  650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F2FAB2F
P 825 825
F 0 "H2" H 925 871 50  0000 L CNN
F 1 "MountingHole" H 925 780 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 825 825 50  0001 C CNN
F 3 "~" H 825 825 50  0001 C CNN
	1    825  825 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F2FAD38
P 825 1000
F 0 "H3" H 925 1046 50  0000 L CNN
F 1 "MountingHole" H 925 955 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 825 1000 50  0001 C CNN
F 3 "~" H 825 1000 50  0001 C CNN
	1    825  1000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F2FAFFD
P 825 1175
F 0 "H4" H 925 1221 50  0000 L CNN
F 1 "MountingHole" H 925 1130 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 825 1175 50  0001 C CNN
F 3 "~" H 825 1175 50  0001 C CNN
	1    825  1175
	1    0    0    -1  
$EndComp
NoConn ~ 5950 3100
NoConn ~ 5950 2800
NoConn ~ 5050 3100
NoConn ~ 5050 3550
NoConn ~ 5050 3700
$Comp
L Switch:SW_Push SW4
U 1 1 5F35F338
P 4100 4770
F 0 "SW4" H 4100 5055 50  0000 C CNN
F 1 "lights" H 4100 4964 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H8mm" H 4100 4970 50  0001 C CNN
F 3 "~" H 4100 4970 50  0001 C CNN
	1    4100 4770
	1    0    0    -1  
$EndComp
Connection ~ 3900 4770
Wire Wire Line
	3900 4770 3900 4925
Text Label 4300 4770 0    50   ~ 0
lights
Text Label 5950 3850 0    50   ~ 0
lights
$Comp
L MainWorking:ArduinoProMini_a U2
U 1 1 5F298F87
P 5500 2650
F 0 "U2" H 5500 2817 50  0000 C CNN
F 1 "ArduinoProMini_a" H 5500 2726 50  0000 C CNN
F 2 "MainWorking:ArduinoProMini_a" H 5500 2650 50  0001 C CNN
F 3 "" H 5500 2650 50  0001 C CNN
	1    5500 2650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
