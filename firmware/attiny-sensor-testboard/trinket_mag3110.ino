#include <Arduino.h>
#include <Wire.h>
#include <SparkFun_MAG3110.h>

MAG3110 mag = MAG3110();

#define SDA 0    //IIC pins for Adafruit Trinket (attiny85)
#define SCL 2

int deviation = 15;  // starting at 15 for the new sensor
int16_t x,y,z;
int oldx,oldy,oldz;
int const bufferLen = 8;

int xbuffer[bufferLen]; // initalize circular buffer for XYZ averaging of reads.
int ybuffer[bufferLen];
int zbuffer[bufferLen];
int xaverage;
int yaverage;
int zaverage;

//bool alarm_lock = false; // a flag to determine if the alarm has been recently activated.
unsigned long alarm_active = 0;          // the time in millis the alarm was last activated
unsigned int alarm_delay = 5000; // time to delay resetting the sensor after activation. This will increase after testing (probably)


void getgauss(){ // averaging of 8 magnetometer readings may be unnecessary for the 3110 sensor. Was absolutly necessary for the earlier, very jittery, QMC5883 sensor.
  xaverage = 0;
  yaverage = 0;
  zaverage = 0;
  int tempxaverage = 0; // temporary storage for the maths
  int tempyaverage = 0;
  int tempzaverage = 0;
  for (int i; i<=bufferLen; i++)
  {
    if(mag.dataReady())
    {
      mag.readMag(&x, &y, &z);
    }
    xbuffer[i] = x; // add new readings to buffer
    ybuffer[i] = y;
    zbuffer[i] = z;
    delay(30);    //lets try 8 reads every 1/2 second
  }
  for (int t; t<=bufferLen; t++)   //average everything into a single number for x, y, z
  {
    xaverage = xbuffer[t] + xaverage;
    yaverage = ybuffer[t] + yaverage;
    zaverage = zbuffer[t] + zaverage;
  }
    tempxaverage = xaverage/bufferLen;
    tempyaverage = yaverage/bufferLen;
    tempzaverage = zaverage/bufferLen;

    xaverage = abs(tempxaverage); //absolute value taken to measure difference from last reading, positive or negative is not important for our use
    yaverage = abs(tempyaverage);
    zaverage = abs(tempzaverage);
    //just printing the numbers to watch for changes while testing with magnet near sensor
    //Serial.print("Averages: "); Serial.print(xaverage); Serial.print(" "); Serial.print(yaverage); Serial.print(" "); Serial.println(zaverage);
}

void blinker(){
  digitalWrite(4, HIGH); // single pin output to other MCU. will stay on for ~2 seconds while led blinks in FOR loop
  for (int i; i < 15; i++){
    digitalWrite(1, HIGH);
    delay(75);
    digitalWrite(1, LOW);
    delay(75);
  }
  digitalWrite(4, LOW);
}

void setup() {
  pinMode(1, OUTPUT); //onboard LED
  pinMode(4, OUTPUT); //pin for hi/lo output to other MCU, pulled down with external 10k
  blinker(); //blink on bootup
  Wire.begin();
  digitalWrite(SDA, LOW); //turn off internal 20k pullups, using external 3k3 pullups
  digitalWrite(SCL, LOW);
//  Serial.begin(9600);
//  Serial.println("starting up");
  mag.initialize(); //Initializes the mag sensor - this call will set wire frequency to 400khz if you do not change it in the library!!!!
  mag.start();      //Puts the sensor in active mode
  digitalWrite(SDA, LOW); //turn off internal 20k pullups, using external 3k3 pullups
  digitalWrite(SCL, LOW);

  getgauss();
  oldx = xaverage;
  oldy = yaverage;
  oldz = zaverage;
  delay(500);
}

void loop() {
  getgauss(); // Get current magnetometer readings
  if (((xaverage-oldx > deviation) || (yaverage-oldy > deviation) || (zaverage-oldz > deviation)))
  {
    blinker();
    alarm_active = millis(); // reset alarm delay, to prevent constant triggering from the same slow vehicle
    delay(alarm_delay); // delay before re-reading the sensor and re-zeroing magnetometer values. This will allow time for the vehicle to pass or stop near the sensor. The alarm will not continuously trip if the vehicle remains in range of the sensor. The alarm WILL trip when the vehicle moves again.
    getgauss();
    oldx = xaverage;
    oldy = yaverage;
    oldz = zaverage;
  }
  delay(50);
}
